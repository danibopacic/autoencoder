import numpy as np
import tensorflow as tf
from tensorflow.keras import layers, Model
import matplotlib.pyplot as plt
from skimage.io import imshow_collection, imread_collection
from tensorflow.keras.callbacks import EarlyStopping

# PRIPREMA PODATAKA
col_dir = 'C:/Users/danib/Desktop/PSU/dataset/dataset/*.jpg'
col_dir2 = 'C:/Users/danib/Desktop/PSU/dataset/dataset/noise/*.jpg'
col = imread_collection(col_dir)
col2 = imread_collection(col_dir2)

train_data = np.array(col)
test_data = np.array(col)

noisy_train_data = np.array(col2)
noisy_test_data = np.array(col2)

# Pokazivanje 10 slika običnih i blurovanih
imshow_collection(train_data)
imshow_collection(noisy_train_data)

# KREIRANJE AUTOENCODERA KORIŠTENJEM FUNCTIONAL API
input = layers.Input(shape=(28, 28, 1))

# Encoder
x = layers.Conv2D(32, (3, 3), activation="relu", padding="same")(input)
x = layers.MaxPooling2D((2, 2), padding="same")(x)
x = layers.Conv2D(32, (3, 3), activation="relu", padding="same")(x)
x = layers.MaxPooling2D((2, 2), padding="same")(x)
x = layers.Conv2D(64, (3, 3), activation="relu", padding="same")(x)
x = layers.MaxPooling2D((2, 2), padding="same")(x)

# Decoder
x = layers.Conv2DTranspose(64, (3, 3), strides=2, activation="relu", padding="same")(x)
x = layers.Conv2DTranspose(32, (3, 3), strides=2, activation="relu", padding="same")(x)
x = layers.Conv2D(1, (3, 3), activation="sigmoid", padding="same")(x)

# Autoencoder
autoencoder = Model(input, x)
autoencoder.compile(optimizer="adam", loss="binary_crossentropy")
autoencoder.summary()

# Treniranje autoenkodera koristeći train_data kao naše ulazne podatke i cilj
early_stopping = EarlyStopping(monitor='val_loss', patience=3, restore_best_weights=True)

autoencoder.fit(
    x=train_data,
    y=train_data,
    epochs=5,
    batch_size=32,
    shuffle=True,
    validation_data=(test_data, test_data),
    callbacks=[early_stopping]
)

# Predviđanje na testnom skupu podataka i prikaz izvornih slika zajedno s predviđanjem autoenkodera.
predictions = autoencoder.predict(test_data)
imshow_collection(test_data)
imshow_collection(predictions)

# Prekvalifikacija autoenkodera koristeći "bucne" tj. blurovane podatke kao unos i čiste podatke kao metu.
# Ovdje enkoder treba naučiti kako izoštriti slike
autoencoder.fit(
    x=noisy_train_data,
    y=train_data,
    epochs=5,
    batch_size=32,
    shuffle=True,
    validation_data=(noisy_test_data, test_data),
    callbacks=[early_stopping]
)

# Predviđanje bučnih tj. blurovanih podataka i prikaz rezultata autoenkodera.
predictions = autoencoder.predict(noisy_test_data)
imshow_collection(noisy_test_data)
imshow_collection(predictions)

plt.show()  # Dodano za prikaz grafova i slika
